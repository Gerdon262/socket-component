const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = {
    outputDir: 'dist',
    chainWebpack: config => {
        config.plugins.delete('html');
    },
    configureWebpack: {
        plugins: [
            new TerserWebpackPlugin()
        ]
    }
};
